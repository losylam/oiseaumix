#include "testApp.h"

void testApp::setup() {
  ofSetVerticalSync(true);
  //ofBackground(0);
  ofColor col1;
  ofColor col2;
  col1.set(0);
  col2.set(90);
  
  width = 640;
  height = 480;
  cam.initGrabber(width, height);
  logo.loadImage("of.png");
  im1.allocate(width , height, OF_IMAGE_COLOR);
  for (int i = 0; i<3; i++){
    dec[i].im.allocate(width , height, OF_IMAGE_COLOR);
    dec[i].x = width*i/3.0;
    dec[i].y = 0;
    dec[i].width = width/3.;
    dec[i].height = height;
    dec[i].state = 0;
  }

  for (int i=0; i<5;i++){
    imgs[i][0].loadImage("bec"+ofToString(i+1)+".png");
    imgs[i][1].loadImage("plume"+ofToString(i+1)+".png");
    imgs[i][2].loadImage("pattes"+ofToString(i+1)+".png");
  }

  draw_input = false;
  draw_output = true;

  swipe = false;

  sender.setup(HOST, PORT);
}

void testApp::update() {
  cam.update();
  string st;
  if(cam.isFrameNew()) {
    for (int i = 0; i<3; i++){
      dec[i].im.setFromPixels(cam.getPixelsRef());
      dec[i].im.crop(dec[i].x, dec[i].y, dec[i].width, dec[i].height);
      ofxZxing::Result curResult = ofxZxing::decode(dec[i].im.getPixelsRef(), true);
      float curTime = ofGetElapsedTimef();
      if(curResult.getFound()){
	dec[i].result = curResult;
	dec[i].lastFound = curTime;
	dec[i].b_result = true;
	dec[i].value = curResult.getText();
	st = dec[i].value.substr(dec[i].value.size()-1,1);
	dec[i].state = ofToInt(st);
      }else if(curTime - dec[i].lastFound > 1){
	dec[i].result = curResult;
	dec[i].b_result = false;
	dec[i].state = 0;
      }
      if (dec[i].state != dec[i].old_state){
	ofxOscMessage m;
	
	m.setAddress("/"+ dec[i].value.substr(0,dec[i].value.size()-1));
	//m.setAddress("/bec");
	m.addIntArg(dec[i].state);
	
	ofLogNotice(ofToString(i) + " change :  " + ofToString(dec[i].state));
	sender.sendMessage(m);

	dec[i].old_state = dec[i].state;
      } 
    }
  }

  if (ofGetFrameNum() == 60){
    ofLog(OF_LOG_NOTICE, "listening for osc messages on port %i",PORT_RECEIVER);
    oscReceiver.setup(PORT_RECEIVER);
  }

  while( oscReceiver.hasWaitingMessages() ){
    ofxOscMessage m;
    string s_osc;
    oscReceiver.getNextMessage( &m );
    s_osc = m.getAddress();
    for (int i=0; i<m.getNumArgs();i++){
      s_osc += " " + m.getArgAsString(i);
    } 
    ofLog(OF_LOG_VERBOSE, "get osc message");
    //    ofLog(OF_LOG_VERBOSE, m.getAddress());
    ofLog(OF_LOG_VERBOSE, s_osc);
    int note;
    if (m.getAddress() == "/swipetop"){
      ofLogNotice("swipe");
      swipe = true;
    }else if(m.getAddress() == "/swipedown"){
      swipe = false;

    }
  }

}

void testApp::draw() {

  ofBackgroundGradient(ofColor(100), ofColor(0), OF_GRADIENT_CIRCULAR);
  ofSetColor(255);
  if (draw_input){
    cam.draw(0, 0);
    
    ofPushStyle();
    ofSetColor(0, 0, 0);
    for (int i = 0; i<3; i++){
      ofLine(width*(i+1)/3, 0, width*(i+1)/3, height); 
      ofPushMatrix();
      ofTranslate(dec[i].x, dec[i].y);
      if (dec[i].b_result){
	dec[i].result.draw();
      ofDrawBitmapString(dec[i].value, 10, height+20);
      }
      ofPopMatrix();
    }
    ofPopStyle();
  }
  if (draw_output){
    if (swipe){
      for (int i=2; i>-1 ; i--){
	if (dec[i].state != 0){
	  imgs[dec[i].state-1][i].draw(420,0, 1080, 1080);
	}
      }
    }
  }
}

void testApp::keyPressed(int key){ 
  switch (key) {
  case 'i':
    draw_input = !draw_input;
    break;
  case 'o':
    draw_output = !draw_output;
    break;
  case 's':
    swipe = !swipe;
    break;
  case 'f':
    ofToggleFullscreen();
    break;
  }
}
