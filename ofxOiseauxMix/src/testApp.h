#pragma once

#include "ofMain.h"
#include "ofxZxing.h"
#include "ofxOsc.h"

#define HOST "localhost"
#define PORT 12345
#define PORT_SC 12347
#define PORT_RECEIVER 12346


class Decoder{
public:
ofImage im;
int x;
int y;
int width;
int height;
float lastFound;
string value;
int state;
int old_state;
bool b_result;
ofxZxing::Result result;
};


class testApp : public ofBaseApp {
 public:
  void setup();
  void update();
  void draw();
  void keyPressed(int key);  

  ofxOscSender sender;
  ofxOscReceiver oscReceiver;
  
  ofxZxing::Result result1;
  ofxZxing::Result result2;
  ofxZxing::Result result3;
  ofImage logo;
  ofVideoGrabber cam;
  float lastFound;
  ofImage im1;
  ofImage im2;
  ofImage im3;
  int width;
  int height;
  ofImage imgs[5][3];
  
  Decoder dec[3];

  bool swipe;

  bool draw_input;
  bool draw_output;
};
