var lst_key;
var lst_images;
var n_images;
var lst_partie;
var b_bec, b_plume, b_pattes;
var n_bec, n_plume, n_pattes;
var texte;
var you_can_swipe;

texte = [ 
	{"bec" :
		{
		"science" : "Les Passériformes sont souvent insectivores pendant la période de nidification, mais en général, ils consomment des insectes et des graines, ainsi que des fruits et des baies à l’automne.",
		"nom" : "Co",
		"biographie" : "Il émet des chants superbes et variés qui font de lui une grande star des scènes de chant."
		}
	,
	"plumes" :		
		{
		"science" : "La couleur bleu turquoise du plumage du mâle est amplifiée par sa capacité à refléter la lumière qui lui donne alors un aspect métallique et brillant.",
		"nom" : "tin",
		"biographie" : "Fier de son plumage coloré, il l'utilise aussi pour faire de la musique et imiter le son de la crécelle."
		}
	,
	"pattes" : 
		{"science" : "La patte est composée de 4 doigts dont 3 à l'avant et 1 à l'arrière. Ce qui correspond à la position la  plus répandue parmi les oiseaux chanteurs, les percheurs et les rapaces.",
		 "nom" : "ga",
		"biographie" : "Ses longues pattes bleues font du §§§nom un danseur étoile exceptionnel."
		}
	}
,
	{"bec" :
		{
		"science" : "Le bec est large et fort. Il permet d'attraper des crustacés, insectes lacustres,...",
		"nom" : "Mela",
		"biographie" : "Fin gourmet, il aime à se préparer des menus avec des crevettes ou des coquillages."
	
	},
	"plumes" :		
		{
		"science" : "Les oiseaux de la famille des Ansériformes vivent en milieu aquatique, leurs plumes imperméables leurs permettent de s'adapter à ce milieu.",
		"nom" : "lani",
		"biographie" : "Avec son costume imperméable, il aime chanter et danser sous la pluie"
		}
	,
	"pattes" :
		{"science" : "Les pattes palmées offrent un déplacement plus sûr dans l'eau que sur terre. Les pattes palmées sont constituées d'une membrane reliant chaque orteil, elles permettent de nager et parfois même de plonger.",
		"nom" : "ta",
		"biographie" : "Le §§§nom aime l'eau et vivre au bord des lacs où il apprécie de se tremper les pattes."
		}
	}
,
	{"bec" :
		{
		"science" : "Le bec long et courbé permet à l'oiseau de fouiller dans la vase pour attraper des crustacés ou des vers et de filtrer les petits animaux.",
		"nom" : "Recur",
		"biographie" : "Il fait de la couture en utilisant son bec long, courbé et rigide comme une aiguille."
		}
	,
	"plumes" :
		
		{
		"science" : "Le plumage est caractérisé par deux bandes noires distinctes sur le manteau et au niveau de la tête et de la nuque.",
		"nom" : "curvi",
		"biographie" : "Il vit en colonie, et aime jouer aux dames avec ses copains."
		}
	,
	"pattes" : 
		{"science" : "Ces longues pattes fines sont caractéristiques des échassiers qui se nourrissent en eau assez profonde.",
		"nom" : "rostra",
		"biographie" : "Ses longues pattes bleues font du §§§nom un danseur étoile exceptionnel."
		}
	}
, 
	{"bec" :
		{
		"science" : "Le bec crochu est une arme redoutable pour attraper de petits rongeurs et les tuer d'un coup de bec.",
		"nom" : "Stri",
		"biographie" : "."
	
	},
	"plumes" :		
		{
		"science" : "Les plumes sont légères, solides et flexibles, très nombreuses elles donnent l'aspect d'une silhouette ronde et trapue.",
		"nom" : "tri",
		"biographie" : "Couvert de son manteau chaud et douillet, il aime affronter le froid et faire du patin à glace."
		}
	,
	"pattes" :
		{"science" : "Les plumes épaisses couvrant les pattes jusqu'aux griffes donne la possibilité de faire un vol silencieux et de fondre sur sa proie, puis de saisir rapidement les proies avec les serres.",
		"nom" : "trix",
		"biographie" : "A pattes de velours, le §§§nom se glisse en silence dans la maison de ses voisins."
		}
	}
,
	{"bec" :
		{
		"science" : "Un bec fort, large à la base et pointu à l'extrémité permet de casser et ouvrir des graines très dures et des noyaux de fruits.",
		"nom" : "Car",
		"biographie" : "Il adore manger des céréales au petit déjeuner sur lesquelles il ajoute de la sève d'érable. Et le soir il préfère les fruits ."
	
	},
	"plumes" :		
		{
		"science" : "Le plumage du mâle est d'un rouge flamboyant qui rappelle la robe d'un cardinal.",
		"nom" : "dina",
		"biographie" : "Tout de rouge vêtu, comme un super héros masqué, il se lance dans les airs pour faire respecter la loi."
		}
	,
	"pattes" :
		{"science" : "La patte est composée de 4 doigts : 3 à l'avant et 1 à l'arrière. Cela permet de pouvoir d'avoir de bonnes prises sur les branches.",
		"nom" : "lis",
		"biographie" : "Equipé de super doigts très agiles, le §§§nom se promène de branche en branche d'où il observe ses voisins."
		}
	}
]


lst_partie = ["bec", "plume", "pattes"];
lst_key = 'azertyuqsdfghj';

n_images = 5;

$("<div/>", {
      "id":"icons-container",
      "class":"icons-container"
  }).appendTo("body");

$("<div/>", {
      "id":"science-container",
      "class":"science-container"
  }).appendTo("body");

$("<div/>", {
      "id":"bio-container",
      "class":"bio-container"
  }).appendTo("body");

$("<div/>", {
      "id":"bio-surtitre",
      "class":"bio-bio"
  }).appendTo("#bio-container");


$("<div/>", {
      "id":"bio-titre",
      "class":"bio-titre"
  }).appendTo("#bio-container");

$("<div/>", {
      "id":"bio-bio",
      "class":"bio-bio"
  }).appendTo("#bio-container");

$("<div/>", {
      "id":"bio-bio-1",
      "class":"bio-bio"
  }).appendTo("#bio-bio");
$("<div/>", {
      "id":"bio-bio-2",
      "class":"bio-bio"
  }).appendTo("#bio-bio");
$("<div/>", {
      "id":"bio-bio-3",
      "class":"bio-bio"
  }).appendTo("#bio-bio");

$("<div/>", {
      "id":"bio-bio-swipe",
      "class":"bio-bio"
  }).appendTo("#bio-container");


var n_img =12;
var width_m = 200;
var padding = 6;

var container = $('.icons-container'); 


for (var partie=0; partie<3; partie++){
    var part = lst_partie[partie];
    
    $("<div/>", {
	  "id":"texte-"+part,
	  "class":"texte-science"
      }).appendTo("#science-container");
    
    for (var i=0; i<n_images;i++){
	container.append($("<div/>", {
			       "id":"img-item-"+part+"-" +(i+1),
			       "class":"img-item img-"+part
			   }));
    	$("#img-item-"+part+"-"+(i+1)).append($("<img/>",{
						    "class":"png-item",
						    "src":"img/"+part+(i+1)+".png"
						    //"width": width_c-padding+"px"
						}));
    }
}

socket = io.connect('http://192.168.1.174', { port: 8080, rememberTransport: false});
console.log('oi');
socket.on('connect', function() {
    // sends to socket.io server the host/port of oscServer
    // and oscClient
    socket.emit('config',
		{
                    server: {
			port: 12345,
			host: '192.168.1.174'
                    },
                    client: {
			port: 12346,
			host: '192.168.1.174'
                    }
		});
});

socket.emit('message', '/sendall');

socket.on('message', function(obj) {
	      var status = document.getElementById("status");
	      var message = obj[2];
	      status.innerHTML = obj[2];
	      
	      console.log(message[0] + " " + message[1]);
	      
	      if (message[0] == "/bec"){
		  $(".img-bec").css("opacity", "0");
		  $(".img-bec").css("left", "-50%");
		  if (message[1]!='0'){
		      b_bec = 1;
		      n_bec = parseInt(message[1])-1;
		      $("#img-item-bec-"+message[1]).animate({opacity:1, left:0}, 1000);
		      $("#texte-bec").text(texte[parseInt(message[1])-1]["bec"]["science"]);
		      $("#texte-bec").animate({opacity:1}, 1000);
		  }else{
		      b_bec=0;   
		      $("#texte-bec").animate({opacity:0}, 1000);
		  }
	      }
	      
	      if (message[0] == "/plume"){
		  $(".img-plume").css("opacity", "0");
		  $(".img-plume").css("left", "-50%");
		  if (message[1]!='0'){
		      $("#img-item-plume-"+message[1]).animate({opacity:1, left:0}, 1000);
		      b_plume = 1;
		      n_plume = parseInt(message[1])-1;
		      $("#texte-plume").text(texte[parseInt(message[1])-1]["plumes"]["science"]);
		      $("#texte-plume").animate({opacity:1}, 1000);
		  }else{
		      b_plume=0;   
		      $("#texte-plume").animate({opacity:0}, 1000);
		  }
	      }
	      
	      if (message[0] == "/pattes"){
		  //	  $(".img-pattes").css("display", "none");
		  $(".img-pattes").css("opacity", "0");
		  $(".img-pattes").css("left", "-50%");
		  if (message[1]!='0'){
		      //     $("#img-item-pattes-"+message[1]).css("display", "block");	
		      $("#img-item-pattes-"+message[1]).animate({opacity:1, left:0}, 1000);
		      b_pattes = 1;
		      n_pattes = parseInt(message[1])-1;
		      $("#texte-pattes").text(texte[parseInt(message[1])-1]["pattes"]["science"]);
		      $("#texte-pattes").animate({opacity:1}, 1000);

		  }else{
		      b_pattes=0;   
		      $("#texte-pattes").animate({opacity:0}, 1000);
		  }
	      }

	      if (b_pattes + b_plume + b_bec == 3){
		  $("#bio-surtitre").text("Veux-tu créer le");
		  $("#bio-titre").text(creer_nom(n_bec, n_plume, n_pattes) + " ?");
		  $("#bio-surtitre").animate({opacity:1}, 3000);	  	  
		  $("#bio-titre").animate({opacity:1}, 3000);	  

		  $("#bio-bio-1").text(texte[n_bec]["bec"]["biographie"].replace('§§§nom', creer_nom(n_bec, n_plume, n_pattes)));
		  $("#bio-bio-2").text(texte[n_plume]["plumes"]["biographie"].replace('§§§nom', creer_nom(n_bec, n_plume, n_pattes)));
		  $("#bio-bio-3").text(texte[n_pattes]["pattes"]["biographie"].replace('§§§nom', creer_nom(n_bec, n_plume, n_pattes)));
				       
		  $("#bio-bio-swipe").text("Glisse ton doigt vers le haut pour faire apparaître ton oiseau")
		  $("#bio-bio-swipe").animate({opacity:1}, 3000);	  
		  you_can_swipe = true;
	      }else{
		  $("#bio-titre").css("opacity", "0");	  
		  $(".bio-bio").css("opacity", "0");	  
		  $("#bio-bio-swipe").css("opacity","0");	  
		  you_can_swipe = false;

		  socket.emit('message', {
				  oscType: "message",
				  address: "/swipedown"
			      });
		  
	      }
	  });

window.onclick = function(e){
    console.log(e.screenX);
    sendPlot(e.clientX, e.clientY, 56);
}

$(window).keydown(function(event){
		      console.log("keydown");
});

$(window).swipe({
   swipeUp:function(event, direction, distance, duration, fingerCount){
       if (b_bec + b_plume + b_pattes){
	   socket.emit('message', {
			   oscType: "message",
			   address: "/swipetop"
		       });
       }
       $("#bio-surtitre").text("La vie du");
       $("#bio-titre").text(creer_nom(n_bec, n_plume, n_pattes));	  
       $(".bio-bio").animate({opacity:1}, 2000);
       $("#bio-bio-swipe").animate({opacity:0}, 2000);


       //$(".icons-container").css("background", "#111");
   },
   swipeDown:function(event, direction, distance, duration, fingerCount){
       $(".icons-container").css("background", "#ddd");
       socket.emit('message', {
		       oscType: "message",
		       address: "/swipedown"
		   });
   }
});

creer_nom = function(n_bec, n_plumes, n_pattes){
    var out;
    out = texte[n_bec]["bec"]["nom"] + texte[n_plumes]["plumes"]["nom"] + texte[n_pattes]["pattes"]["nom"];
    return out;
};

creer_bio = function(n_bec, n_plumes, n_pattes){
    var out;
    out = texte[n_bec]["bec"]["biographie"] +"\n"
		+    texte[n_plumes]["plumes"]["biographie"] +"\n"
		+    texte[n_pattes]["pattes"]["biographie"];
    out = out.replace('§§§nom', creer_nom(n_bec, n_plumes, n_pattes));
    out = out.replace('§§§nom', creer_nom(n_bec, n_plumes, n_pattes));
    out = out.replace('§§§nom', creer_nom(n_bec, n_plumes, n_pattes));
    return out;
};


